Olen laskennallinen fyysikko, joka tykkää kaikenlaisista ohjelmoinnista työssä ja vapaa-ajalla. Mielestäni ohjelmoinnin kuuluisi olla mahdollisimman helppoa ja hauskaa mahdollisimman monelle ja siksi haluan tehdä ymmärrettäviä ohjelmointioppaita. 

Erityisesti pidän erilaisten systeemien mallintamisesta, palvelimista ja automatisoinnista. Peli- ja GPU-ohjelmointiakin on tullut kokeiltua. Suosikkikieliäni ovat Python ja Rust, mutta minulta taittuu myös mm. C++, JavaScript, Fortran ja MATLAB. 
